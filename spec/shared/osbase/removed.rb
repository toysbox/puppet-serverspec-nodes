shared_examples 'osbase::removed' do

  context 'osbase on Redhat', :if => os[:family] == 'redhat' do
    context 'osbase::packages::remove on Redhat' do
      describe 'telnet-server tn5250 dcap-tunnel-telnet' do
        it { expect(package('telnet-server')).to_not be_installed }
        it { expect(package('tn5250')).to_not be_installed }
        it { expect(package('dcap-tunnel-telnet')).to_not be_installed }
      end
    end
  end

  context 'osbase on Debian', :if => os[:family] == 'debian' do
    context 'osbase::packages::remove on Debian' do
      describe 'telnet-server, telnetd-ssl, telnetd, krb5-telnetd, inetutils-telnetd, heimdal-servers' do
        it { expect(package('telnet-server')).to_not be_installed }
        it { expect(package('telnetd-ssl')).to_not be_installed }
        it { expect(package('telnetd')).to_not be_installed }
        it { expect(package('krb5-telnetd')).to_not be_installed }
        it { expect(package('inetutils-telnetd')).to_not be_installed }
        it { expect(package('heimdal-servers')).to_not be_installed }
      end
    end
  end

  context 'osbase on all OS' do

  end
end
