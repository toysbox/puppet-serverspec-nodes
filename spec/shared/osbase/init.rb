shared_examples 'osbase::init' do

  context 'osbase on Redhat', :if => os[:family] == 'redhat' do
  end

  context 'osbase on Debian', :if => os[:family] == 'debian' do
  end

  context 'osbase on all OS' do
  end

end
